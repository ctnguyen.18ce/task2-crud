<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="../css/js/jquery.slim.min.js">
    <link rel="stylesheet" href="../css/js/popper.min.js">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <title>Home</title>
</head>
<body>
<?php
include_once('header.php'); 
?>
<div class="container">
    <h2 >Post Management</h2>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <button class="btn btn-success" onclick="location.href='./create.php'" id="btn_add" style="margin-bottom: 30px; margin-left :5px;">+ Add Post</button>
            </div>
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
                <form action="" method="get" class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" value="<?=isset($_GET['search']) ? $_GET['search'] : "";?>" name="search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="submit" >Search</button>
                </form>
            </div>
        </div>
    </div>
    <table class="table table-striped table-bordered">
        <thead style="text-align: center;">
            <tr>
                <th width="5%">ID</th>
                <th width="25%">Title</th>
                <!-- <th>Status</th> -->
                <th width="35%">Content</th>
                <th width="10%">Author</th>
                <th width="10%%">Creation Time</th>
                <th width="15%">Options</th>
            </tr>
        </thead>
</div>
<?php
include_once ('../models/handle.php');
    $handle = new CRUD();
    // mysqli_set_charset($conn,"utf8");
    session_start();
    if(!isset($_SESSION["id"])){
        header('Location: ../views/index.php');
    }
    $search = addslashes($_GET['search']) ? $_GET['search'] : '';
    $limit = !empty($_GET['per_page']) ? $_GET['per_page'] : 5;
    
    if(isset($_GET['page']) && (int)$_GET['page'] != 0){
        $current_page = $_GET['page'];
    } else {
        $current_page = 1;
    }
    $offset = ($current_page - 1) * $limit;
    if (empty($search)) {
        $handle->select('posts','id,title,LEFT(content,400) as content,author,cre_time','','id DESC',$limit,$offset);
        $result = $handle->query;
        $handle->select('posts','*');
        $result1 = $handle->query;
    } 
    else {
        $handle->select('posts','id,title,LEFT(content,400) AS content,author,cre_time','title LIKE "%'.$search.'%" OR content LIKE "%'.$search.'%" OR author LIKE "%'.$search.'%"','id DESC',$limit, $offset);
        $result = $handle->query;
        $handle->select('posts','*','title LIKE "%'.$search.'%" OR content LIKE "%'.$search.'%" OR author LIKE "%'.$search.'%"');
        $result1 = $handle->query;
    }
    $total_records = $result1->num_rows;
    // $current_page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
   
    $total_pages = ceil($total_records / $limit);

    
        
    if ($total_records > 0) {
        while ($row = $result->fetch_array()) {
?>
<tbody>
    <tr>
        <td><?php echo $row['id'] ?></td>
        <td><b><?php echo $row['title'] ?></b></td>
        <td ><?php echo $row['content'] ?>...</td>
        <td><?php echo $row['author'] ?></td>
        <td><?php echo $row['cre_time'] ?></td>
        <td style="text-align: center;"><a href="read.php?id=<?php echo $row['id']; ?>"><i class="fa fa-eye" style='font-size:24px'></i></a>&ensp;&ensp;
        <a href="update.php?id=<?php echo $row['id']; ?>"><i class="fas fa-edit" style='font-size:24px'></i></a>&ensp;&ensp;
        <a onclick="delete_confirm(<?php echo $row['id']?>);" href="javascript:void(0)"><i class="fa fa-trash" style='font-size:24px'></i></a></td>
        
    </tr>
    
</tbody>

<?php
$prev = $current_page - 1;
$next = $current_page + 1;
}
}else {
    echo "<tbody><tr><td colspan ='6' style = 'text-align :center;'><p> No records were found</p></td><tbody><tr>";
}

// timeout
if(isset($_SESSION['timeout']) ) {
    $session_life = time() - $_SESSION['timeout'];
    if($session_life > 300)
        { 
            header("Location: ../models/logout.php"); 
        }
    }
    $_SESSION['timeout'] = time();
?>
        </table>
<?php
if($total_records > 0){
    include_once('footer.php');
}
?>
<script>
        function delete_confirm(id) {
            let choice  = confirm("Are you sure you want to delete post ?");
            if (choice == true) {
                window.location = 'http://localhost/task2/controllers/delete.php?id='+ id;
            }
        }
    </script>
</body>
</html>