<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reading Post</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="../css/js/jquery.slim.min.js">
    <link rel="stylesheet" href="../css/js/popper.min.js">
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
</head>
<body>
<?php
include_once('header.php'); 
include_once ('../models/handle.php');
session_start();
if(!isset($_SESSION["id"])){
    header('Location: ../views/index.php');
}
    $id = $_GET['id'];
    $handle = new CRUD();
    $handle->select('posts','*',"id ='$id'");
    $result = $handle->query;
    while( $row = $result->fetch_array(MYSQLI_ASSOC))
    {
        $title = $row["title"];
        $content = $row["content"];
        $author = $row["author"];
        $cre_time = $row["cre_time"];
?>
<div class="container">
    <div class="form-group" style="margin-left: 50px;margin-top: 50px;">
        <h2><?php echo $title; ?></h2>
    </div>
    <div class="form-group" style="margin-left: 50px;">
        <p><?php echo $cre_time;?> <?php echo "by: "; echo $author; ?></p>
    </div>
    <br>
    <div class="form-group" style="margin-left: 100px;margin-right: 70px;">
        <p class="form-control-static"><?php echo $content; ?></p>
    </div>
    <!-- <div class="form-group" style="text-align: center;">
        <img  src='<?php echo $images; ?>' width='550' height='350'>
    </div> -->
    <div class="form-group" style="text-align:center;">
        <p><a href="home.php" class="btn btn-primary" id="btnback1">Back</a></p>
    </div>
</div>
    <?php
   }
   ?>
</body>
</html>