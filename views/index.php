<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="../css/js/jquery.slim.min.js">
    <link rel="stylesheet" href="../css/js/popper.min.js">

</head>
<body>
    <div class="center">
        <h1 style="text-align: center;">Login Management</h1>
        <br>
        <span style="color :red;"><b><?php foreach($error as $err){ echo $err; echo "<br>";} ?></b></span>
        <br><br>
        <form  action="../controllers/login.php" method="post">
            <div class="form-group">
                <label for="usn">Username:</label>
                <input type="text" name="username" class="form-control" placeholder="Enter Username" id="usn" required>
            </div>
            <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" name="password" class="form-control" placeholder="Enter password" id="pwd" required>
            </div>
            <div class="login_btn" style="text-align: center;">
                <button type="submit" name="submit" id="login_btn" class="btn btn-primary">Login</button>
                <p>Don't have an account yet?<a href="../views/register.php">Register here</a></p>
            </div>
        </form>
    </div>
</body>
</html>