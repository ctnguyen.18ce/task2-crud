<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="../css/js/jquery.slim.min.js">
    <link rel="stylesheet" href="../css/js/popper.min.js">
    <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <script type="text/javascript" src="../ckeditor/ckfinder/ckfinder.js"></script>
</head>
<body>
<?php
include_once('header.php'); 
include_once ('../models/handle.php');
session_start();
if(!isset($_SESSION["id"])){
    header('Location: ../views/index.php');
}

    // mysqli_set_charset($db->conn,"utf8");
    $id = trim($_GET["id"]);
    $handle = new CRUD();
    $handle->select("posts","*","id='$id'");
    $result = $handle->query;
    $row = $result->fetch_array();

    if (isset($_POST['update'])) {
        include_once('../controllers/updateController.php');
    }
?>
    <div class="container">
        <h2 style="text-align:center; margin-top:20px;">Update Post</h2>
        <span class="help-block" style="color :red;"><b><?php foreach($error as $err){ echo $err; echo "<br>";}?></b></span>
        <form method="post" action="" enctype="multipart/form-data">
            <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="title" class="form-control" value="<?php echo $row['title']; ?>" required>
            </div>
            <div class="form-group">
                    <label>Author</label>
                    <input type="text" name="author" class="form-control" value="<?php echo $row['author']; ?>"required>
            </div>
            <div class="form-group">
                    <label>Content</label>
            </div>
            <div class="form-group">
                <textarea class="ckeditor" name="content" id="editor" required><?php echo $row['content']; ?></textarea>
                <script>
                    var editor = CKEDITOR.replace( 'content', {height: 480,});
                    CKFinder.setupCKEditor();
            </script>
            </div>
            
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6" style="text-align: right;">
                        <p><a href="home.php" class="btn btn-primary">Back</a></p>
                    </div>
                    <div class="col-sm-6" style="text-align: left;">
                        <button type="submit" name="update" class="btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>