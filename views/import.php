<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import File</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="../css/js/jquery.slim.min.js">
    <link rel="stylesheet" href="../css/js/popper.min.js">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
</head>

<body>
    <div class="container">
    <h3>Import File Users</h3>
        <div class="row">
            <div class="col-sm-6">
                <?php
                // var_dump(phpinfo());die;
                    include_once '../models/Transacsion.php';
                    $startTime = microtime();
                    if (isset($_POST['importSubmit'])) {

                        $target_file = $_FILES["myfile"]["tmp_name"];
                        $handle = fopen ($target_file, "r");
                        $csvMimes = array(
                            'application/x-csv',
                            'text/x-csv',
                            'text/csv',
                            'application/csv',
                        );
                        // check file CSV
                        if (in_array($_FILES['myfile']['type'], $csvMimes)) {
                            echo "</br>";
                            $line=1;
                            $arr = array(); 
                            $users_list = array();
                            if (!empty($handle)) {
                                $obj = new TransactionDB();
                                $result = $obj->importdata($data,$arr,$handle);
                            } 
                            fclose ($handle);
                        } else {
                            echo "<b>File is not CSV</b>";
                        }
                    }
                ?>
            </div>
            <div class="col-6">
                <form action="import.php" method="post" enctype="multipart/form-data">
                    <input type="file" name="myfile" id="myfile"> <br><br><br>
                    <input type="submit" name="importSubmit" value="Import" class="btn btn-success">
                    <br>
                    <br>
                    <p><a href="./users.php" class="btn btn-primary">Back</a></p>
                </form>
            </div>
        </div>
    </div>
</body>

</html>
