<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light"  style="background-color: #e3f2fd; height : 70px;margin-left: 200px;
    margin-right: 200px;">
  <!-- Container wrapper -->
  <div class="container-fluid">

    <!-- Toggle button -->
    <button class="navbar-toggler" type="button" data-mdb-toggle="collapse"
      data-mdb-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
      aria-label="Toggle navigation">
      <i class="fas fa-bars text-light"></i>
    </button>
    <!-- Collapsible wrapper -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <!-- Left links -->
      <ul class="navbar-nav me-auto d-flex flex-row mt-3 mt-lg-0" id="as">
        <li class="nav-item text-center mx-2 mx-lg-1">
          <a class="nav-link active" aria-current="page" href="home.php">
            <div>
            <i class="fa fa-home" ></i>
            </div>
            Home
          </a>
        </li>

        <li class="nav-item text-center mx-2 mx-lg-1">
          <a class="nav-link active " href="./users.php">
            <div>
            <i class="fa fa-user" aria-hidden="true"></i>
            </div>
            Users
          </a>
        </li>
      </ul>
      <!-- Right links -->
    </div>
    <div style="text-align: right;margin-right : 50px;"> 
        <p style="text-align:right;color : black; margin-top :10px;">Welcome,<b><?php session_start(); echo $_SESSION["username"];?></b></p>
        <p><a href="../models/logout.php" title="Logout"><p style="text-align:right;">Logout</p></a></p>
    </div>
    <!-- Collapsible wrapper -->
  </div>
  <!-- Container wrapper -->
</nav>
<!-- <script>
// Add active class to the current button (highlight it)
var header = document.getElementById("navbarSupportedContent");
var btns = header.getElementsByClassName("nav-item");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  current[0].className = current[0].className.replace(" active", "");
  this.className += " active";
  });
}
</script> -->
