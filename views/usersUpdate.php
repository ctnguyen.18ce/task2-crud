<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="../css/js/jquery.slim.min.js">
    <link rel="stylesheet" href="../css/js/popper.min.js">
    <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <script type="text/javascript" src="../ckeditor/ckfinder/ckfinder.js"></script>
</head>
<body>
<?php
include_once('header.php'); 
include_once ('../models/handle.php');
// session_start();
if(!isset($_SESSION["id"])){
    header('Location: ../views/index.php');
}
error_reporting(0);

    // mysqli_set_charset($db->conn,"utf8");
    $id = trim($_GET["id"]);
    $handle = new CRUD();
    $handle->select("users","*","id='$id'");
    $result = $handle->query;
    $row = $result->fetch_array();

    if (isset($_POST['usersUpdate'])) {
        include_once('../controllers/usersUpdateController.php');
    }
?>
    <div class="container" id="usersUpdate">
        <h2 style="text-align:center; margin-top:20px;">Update Users</h2>
        <span class="help-block" style="color :red;"><b><?php foreach($user_error as $err){ echo $err; echo "<br>";}?></b></span>
        <form method="post" action="" enctype="multipart/form-data" style="margin-left: 300px;margin-right : 300px;">
        <div class="form-group">
        <label for="usr">Username:</label>
        <input type="text" name="username" class="form-control" value="<?php echo $row['username']; ?>" id="usr" required>
    </div>
    <div class="form-group">
        <label for="fn">Full name:</label>
        <input type="text" name="fullname" class="form-control" value="<?php echo $row['fullname']; ?>" id="fn" required>
    </div>
    <div class="form-group">
        <label for="eml">Email:</label>
        <input type="email" name="email" class="form-control" value="<?php echo $row['email']; ?>" id="eml" required>
    </div> 
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6" style="text-align: right;">
                        <p><a href="../views/users.php" class="btn btn-primary">Back</a></p>
                    </div>
                    <div class="col-sm-6" style="text-align: left;">
                        <button type="submit" name="usersUpdate" class="btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>