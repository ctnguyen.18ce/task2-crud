<nav aria-label="Page navigation example mt-5">
<?php
$param ='';
if($search){
    $param = "search=".$search ."&";
}
?>
    <ul class="pagination justify-content-center">
    <?php if($current_page == 1){}else {
            $first = 1;
    ?>
        <li class="page-item">
        <a class="page-link" href="<?php echo '?'.$param;?>per_page=<?=$limit?>&page=<?=$first; ?>">First</a>
        </li>
    <?php } ?>
        <li class="page-item <?php if($current_page <= 1){ echo 'disabled'; } ?>">
            <a class="page-link"
                href="<?php if($current_page == 1){ } else{ echo '?'.$param."?per_page=".$limit."&page=".$prev ;} ?>">Previous</a>
        </li>
        <?php for($i = 1; $i <= $total_pages; $i++ ){ ?>
        <li class="page-item <?php if($current_page == $i) { echo 'active'; } if($i > $current_page - 3 && $i < $current_page + 3) {
         ?>">
            <a class="page-link" href="<?php echo '?'.$param.'per_page='.$limit.'&page='.$i; ?>"> <?= $i; ?> </a>
            <?php } ?>
        </li>
        <?php } ?>
        <li class="page-item <?php if($current_page >= $total_pages) { echo 'disabled'; } ?>">
            <a class="page-link"
                href="<?php if($current_page >= $total_pages){ echo ''; } else {echo '?'.$param."per_page=".$limit."&page=". $next; } ?>">Next</a>
        </li>
        <?php if($current_page == $total_pages){ } else { ?>
        <li class="page-item ">
            <a class="page-link" href="<?php echo '?'.$param; ?>per_page=<?=$limit?>&page=<?=$total_pages;?>">Last</a>
            <?php } ?>
        </li>
    </ul>
</nav>