<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="../css/js/jquery.slim.min.js">
    <link rel="stylesheet" href="../css/js/popper.min.js">
    <title>Create New Users</title>
</head>
<body>
<?php
session_start();
if(!isset($_SESSION["id"])){
    header('Location: ../views/index.php');
}
?>
<div class="rgt">
    <h1 style="text-align: center;">Create New Users</h1>
    <span style="color :red;"><b><?php foreach($have_error as $err){ echo $err; echo "<br>";} ?></b></span>
    <span><b><?php echo $successful ?></b></span> 
    <form action="../controllers/createUsersController.php" method="post">
    <br>
    
    <div class="form-group">
        <label for="usr">Username:</label>
        <input type="text" name="username" class="form-control" id="usr" required>
    </div>
    <div class="form-group">
        <label for="fn">Full name:</label>
        <input type="text" name="name" class="form-control" id="fn" required>
    </div>
    <div class="form-group">
        <label for="eml">Email:</label>
        <input type="email" name="email" class="form-control" id="eml" required>
    </div>
    <div class="form-group">
        <label for="pwd">Password:</label>
        <input type="password" name="password" class="form-control" id="pwd" required>
    </div>
    <div class="form-group">
        <label for="pwd1">Retype Password:</label>
        <input type="password" name="password1" class="form-control" id="pwd1" required>
    </div>
    <div class="container">
                <div class="row">
                    <div class="col-sm-6" style="text-align: right;">
                        <p><a href="../views/users.php" class="btn btn-primary">Back</a></p>
                    </div>
                    <div class="col-sm-6" style="text-align: left;">
                        <button type="submit" name="register" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
    </form>
</div>
</body>
</html>