<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Creating</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="../css/js/jquery.slim.min.js">
    <link rel="stylesheet" href="../css/js/popper.min.js">
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../ckeditor/ckfinder/ckfinder.js"></script>
</head>
<body>
<?php
include_once('header.php'); 
include_once('../controllers/createController.php');
session_start();
if(!isset($_SESSION["id"])){
    header('Location: ../views/index.php');
}
?>
<div class="container">
    <h2 style="text-align:center;">Creating Post</h2>
    <?php
        foreach($have_error as $error){
            echo "<b>$error</b><br>";
        }
    ?>
    <br>
    <form method="post" action="" enctype="multipart/form-data">
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Author</label>
            <input type="text" name="author" class="form-control" required>
            
        </div>
        <div class="form-group">
            <label>Content</label>
        </div>
        <div class="form-group">
            <textarea class="ckeditor" id="editor" name="content" required></textarea>
            <script>
                // var editor = CKEDITOR.replace( 'content', {
                // height: 480,
                // filebrowserUploadUrl : "../controllers/createController.php" , //URL upload images
                // filebrowserUploadMethod: 'form' 
                // });
                var editor = CKEDITOR.replace( 'content');
                CKFinder.setupCKEditor();
            </script>
        </div>
        
        <!-- <div class="form-group">
            <input type="file" name="image" id="image">
        </div> -->
        <br>
        <div class="container">
                <div class="row">
                    <div class="col-sm-6" style="text-align: right;">
                        <p><a href="home.php" class="btn btn-primary">Back</a></p>
                    </div>
                    <div class="col-sm-6" style="text-align: left;">
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
    </form>
</div>
</body>
</html>