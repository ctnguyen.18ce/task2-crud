<?php
class TransactionDB {

    const DB_HOST = 'localhost';
    const DB_NAME = 'exercise2';
    const DB_USER = 'root';
    const DB_PASSWORD = '';
    /**
     * Open the database connection
     */
    public function __construct() {
        // open database connection
        $conn = sprintf("mysql:host=%s;dbname=%s", self::DB_HOST, self::DB_NAME);
        try {
            $this->pdo = new PDO($conn, self::DB_USER, self::DB_PASSWORD);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function importdata($data,$arr,$handle){
        try {
            $this->pdo->beginTransaction();

            $have_error = false;
            $line = 1;
            $arr = array();
            $sql = "DELETE FROM users";
            $this->pdo->exec($sql);
            $count = 1;
            $query = "";
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $arr[]=$data;
                if($line == 1 ){
                    // $line++;
                }else{
                    if (empty($data[0])) {
                        echo "Empty at line " . $line;
                        echo "</br>";
                        if (!$have_error) {
                            $have_error = true;
                        }
                    }
                    if (empty($data[2])) {
                        echo "Empty at ". $arr[0][2] ." and ID is ".$data[0];
                        echo "</br>";
                        if (!$have_error) {
                            $have_error = true;
                        }
                    }
                    if (empty($data[4])) {
                        echo "Empty at ". $arr[0][4] ." and ID is ".$data[0];
                        echo "</br>";
                        if (!$have_error) {
                            $have_error = true;
                        }
                    }
                    if ($have_error == true){
                        throw new PDOException();
                    }
                    if (!$have_error){
                        $fullname = $data[1];
                        $username = $data[2];
                        $email = $data[3];
                        $passw = md5($data[4]);

                        if ($count == 1) {
                            $query .= "INSERT INTO users (fullname,username,email,passw) VALUES('". $fullname . "','". $username ."','". $email ."','". $passw ."')";
                        } else {
                            $query .= ",('". $fullname . "','". $username ."','". $email ."','". $passw ."')";
                        }
                        if ($count == 500) {
                            $result = $this->pdo->exec($query);
                            $query = "";
                            $count = 0;
                        }
                        $count++;
                    }
                }
                $line ++;
            }
            
            if($result) {
                echo " Import Successfull";
            } else {
                echo " Import Fails";

            }
            $this->pdo->commit();
        }catch(PDOException $e) {
            $this->pdo->rollBack();
        }
    }
}
?>
