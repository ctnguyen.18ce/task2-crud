<?php
require('Database.php');
class CRUD 
{
    public $query;
    public $result = array();
    public $db = null;
    public $search;
        
    function __construct() 
    {
        $this->db = new Database();
    }

    //insert
    public function insert($table,$param = array()) 
    {
        mysqli_set_charset($this->db->conn,"utf8");
        $table_columns = implode(',', array_keys($param));
        $table_value = implode("','", $param);
        $query = "INSERT INTO $table($table_columns) VALUES('$table_value')";
        $result = $this->db->conn->query($query);
    }
    
    // Delete posts
    public function delete($table,$id) 
    {
        $query = "DELETE FROM $table";
        $query .= " WHERE $id";
        $query;
        $result = $this->db->conn->query($query);
    }

    // Select posts
    public function select($table, $rows, $where = '',$orderby ='', $limit = '',$offset = '') 
    {
        mysqli_set_charset($this->db->conn,"utf8");
        $query = "SELECT $rows FROM $table";
        if($where != ''){
            $query .= " WHERE " . $where;
        }
        if($orderby != ''){
            $query .= " ORDER BY " . $orderby;
        }
        if($limit != ''){
            $query .= " LIMIT " . $limit;
        }
        if($offset != ''){
            $query .= " OFFSET " . $offset;
        }
        
        $this->query = $result = $this->db->conn->query($query);
    }
    
    // Update
    public function update($table,$para=array(),$id) 
    {
        $args = array();
        foreach($para as $key =>$value) {
            $args[] = "$key = '$value'";
        }
        $query = "UPDATE $table SET ". implode(',', $args);
        $query .= " WHERE $id";
        $result = $this->db->conn->query($query);
    }
    
    public function __destruct() 
    {
        $this->db->conn->close();
    }

}
?>