#Task2
ip : 192.168.1.219
URL: cms219.dev2.local

- Truy cập vào URL sẽ xuất hiện page login
- Login bằng username và password lưu trong bảng users của MySQL
- Nếu không có account thì chuyển ra page register để đăng kí account mới, Sau khi đăng nhâp
sẽ chuyển tới page home
- Ở page Home có thể lựa chọn thêm mới, chỉnh sửa, xóa , đọc bài viết đang có
- Khi bấm vào add post sẽ chuyển qua page create post để tạo bài viết mới sau khi submit
- Khi nhấn vào options Edit ở mỗi bài viết thì sẽ chuyển qua page edit post để chỉnh sửa nội dụng
của bài viết đó
- Khi nhấn vào options delete thì sẽ xóa post được chọn.
