<?php
// header('Content-Type: text/html; charset=UTF-8');
include_once ('../models/handle.php');
// mysqli_set_charset($db,"utf8");
if ($_SERVER["REQUEST_METHOD"] == "POST") 
{

    $title = trim($_POST['title']);
    // $status = trim($_POST['status']);
    $content = trim($_POST['content']);
    $author = trim($_POST['author']);
    $have_error = array();
    // check field
    if (!$title || !$content || !$author) 
    {
        $have_error[] = "Don't leave fields blank";
    }

    include_once('ck_uploadController.php');
    
    if (empty($have_error)) 
    {
        $handle = new CRUD();
        $handle->insert('posts',['title'=>$title,'content'=>$content,'author'=>$author]);
        if ($handle == true) 
        {
            header("Location: ../views/home.php");
        }
    }
}

?>