<?php
$title = $status = $content = $author = '';
$title_err = $stt_err = $content_err = $author_err = $image_error = "";
$input_title = trim($_POST['title']);
$input_content = trim($_POST['content']);
$input_author = trim($_POST['author']);
$have_error = false;
$error = array();

// $input_image = $_FILES['image']['name'];
// $input_image_tmp = $_FILES['image']['tmp_name'];
// $image_size = $_FILES['image']['size'];

if (!$input_title) {
    $error[] = "This field title can not be left empty.";
    $have_error = true;
} else {
    $title = $input_title;
}
if (!$input_content) 
{
    $error[] = "This field content can not be left empty.";
    $have_error = true;
} 
else {
    $content = $input_content;
}
if (!$input_author) 
{
    $error[] = "This field author can not be left empty.";
    $have_error = true;
} 
else {
    $author = $input_author;
}

include_once('ck_uploadController.php');

if ($have_error != true)
{
    $handle->update('posts',['title'=>$title,'content'=>$content,'author'=>$author],"id ='$id'");
    if ($handle == true) 
    {
        header("Location: ../views/home.php");
        exit();
    } 
    else {
        echo "Fails";
    }
}
?>