<?php
include_once '../models/Transacsion.php';
    $obj = new TransactionDB();

    $header = array('id','fullname','username','email','password');

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=file_export.csv');

    $output = fopen( 'php://output', 'w' );
    ob_end_clean();

    fputcsv($output, $header);
    $query = "SELECT * FROM users";
    $result = $obj->pdo->prepare($query);
    $result->execute(); 
    $row = $result->fetchAll(PDO::FETCH_ASSOC);
    foreach ($row as $data) {
        fputcsv($output, $data);
    }
    exit();
?>
